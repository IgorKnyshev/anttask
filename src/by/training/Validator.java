package by.training;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.tools.ant.Task;

public class Validator extends Task{
	
	public void execute() {
		Task();
	}
	
	public void Task() {
		int i = 0, n = 0;
		XMLInputFactory factory = XMLInputFactory.newInstance();
		try {
			Reader reader = new FileReader("./build.xml");
			XMLStreamReader r = factory.createXMLStreamReader(reader);
		    int event = r.getEventType();
		    Pattern pat = Pattern.compile("([a-zA-Z-]+)"); 
		    List<String> attrList = new ArrayList<String>();
		    List<String> targets = new ArrayList<String>();
		    List<String> depends = new ArrayList<String>();
		    while (true) {
		    	switch (event) {
		        	case XMLStreamConstants.START_DOCUMENT:
		        		System.out.println("Start Document.");
		                break;
		            case XMLStreamConstants.START_ELEMENT:
		            	String tag = r.getName().toString();
		            	//System.out.println("Start Element: " + tag);
		                for (i = 0, n = r.getAttributeCount(); i < n; ++i) {
		                	String name = r.getAttributeName(i).toString();
		                	String value = r.getAttributeValue(i).toString();
		                	//System.out.println("Attribute: " + name + "=" + value);						//TASK 3
		                	if ("name".equals(name)) {														//checking value of attribute name 
		                        Matcher mat = pat.matcher(value); 											//
		                        if (!mat.matches()) {														//Is there any matches?
		                        	System.out.println(value + " is wrong name! XML file isn't valid!");  	
		                        }																			
		                        else																		
		                        	System.out.println(value +" is correct name.");						
		                    }																				//TASK 2
		                	if("project".equals(tag)) {														//outer <project>
		                		attrList.add(name);	  														//created list with all attributes of <project>
		                	}																				//TASK 1(part or TASK 2)
		                	if ("target".equals(tag) && "name".equals(name)) {								//names of all targets
		                		targets.add(value);															//created list with name values of all targets
		                    }																						
		                    if (("depends".equals(name)) || ("project".equals(tag) && "default".equals(name))) {	//all depends(default attribute of <project>		            
		                    	depends.add(value);																	// is depends too)
		                    }
		                }		                		              
		                break;
		            case XMLStreamConstants.CHARACTERS:
		            	if (r.isWhiteSpace())
		            		break;
		                //System.out.println("Text: " + r.getText());
		                break;
		            case XMLStreamConstants.END_ELEMENT:
		            	//System.out.println("End Element:" + r.getName());
		                break;
		            case XMLStreamConstants.END_DOCUMENT:
		            	System.out.println("End Document.");
		                break;
		            }
		    	
		            if (!r.hasNext())
		                  break;
		            
		            event = r.next();
		    }
		    if (!attrList.contains("default"))									//is attrList of <project> contains attribute default
		    	System.out.println("No default attribute in <project> tag!");
            else
            	System.out.println("Project contains default attribute.");
		    
		    Collections.sort(targets);											//targets - sorted list of all targets			
		    Collections.sort(depends);											//depends - sorted list of all depends
		    boolean isSubset = targets.containsAll(depends);					//is depends subset of all targets? -> all depends exist in targets
		    if (isSubset)
		    	System.out.println("All depends are correct.");
		    else
		    	System.out.println("Depends are wrong!");
		}
		catch(FileNotFoundException | XMLStreamException e) {
			System.out.println(e);
		}	
	}
}